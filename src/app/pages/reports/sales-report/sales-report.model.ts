export class SalesReportFilter {
    public teslimatTipiIdList: string[];
    public cikisDepoIdList: string[];
    public varisDepoIdList: string[];
    public koordinatorIdList: string[];
    public bolgeIdList: string[];
}

export class Report {
    public day: string;
    public mount: string;
    public lastYearNetKargo: number;
    public currentYearNetKargo: number;
    public lastYearPXP: number;
    public currentYearPXP: number;
    public lastYearTotal: number;
    public currentYearTotal: number;
}

export enum ChartViewEnum {
    Day = 1,
    Mount = 2
}