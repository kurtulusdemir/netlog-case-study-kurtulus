import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { SalesReportService } from './sales-report.service';
import { FormControl }  from '@angular/forms';
import { SalesReportFilter, ChartViewEnum, Report } from './sales-report.model';
import { MatButtonToggleGroup, MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { ApexOptions, ChartComponent } from '../../../../@vex/components/chart/chart.component';
import { defaultChartOptions } from 'src/@vex/utils/default-chart-options';
import { createDateArray } from 'src/@vex/utils/create-date-array';

@Component({
  selector: 'vex-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss']
})
export class SalesReportComponent implements OnInit {

  coordionatorList: any[] = [];
  districtList: any[] = [];
  warehouseList: any[] = [];
  deliveryTypeList: any[] = [];

  coordionator = new FormControl();
  district = new FormControl();
  warehouseExit = new FormControl();
  warehouseArrival = new FormControl();
  deliveryType = new FormControl();

  filter: SalesReportFilter = new SalesReportFilter();
  dataSourceReportList = new MatTableDataSource<any>();
  displayedColumns: string[] = ['day', 'lastYearNetKargo', 'currentYearNetKargo', 'lastYearPXP', 'currentYearPXP', 'lastYearTotal', 'currentYearTotal'];
  chartViewEnum = ChartViewEnum;

  @ViewChild('chartViewBtnGroup', {static:true}) chartViewBtnGroup:MatButtonToggleGroup;
  @ViewChild('paginator', {static: true}) paginator: MatPaginator;
  @ViewChild("reportChart", {static:true}) reportChart: ChartComponent;
  chartReportOptions: ApexOptions = defaultChartOptions({
    grid: {
      show: true,
      strokeDashArray: 3,
      padding: {
        left: 16
      }
    },
    chart: {
      type: 'area',
      height: 384,
      sparkline: {
        enabled: false
      },
      zoom: {
        enabled: false
      }
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 90, 100]
      }
    },
    colors: ['#27a0fd', '#23e2a1'],
    labels: null,
    dataLabels: {
      enabled: true,
      enabledOnSeries: true,
      formatter: function(val) {
        return val + " TL";
      },
    },
    xaxis: {
      type: 'category',
      labels: {
        show: true
      },
    },
    yaxis: [{
      labels: {
        show: true,
      },
    },{
      opposite: true,
      labels: {
        show: true,
      },
    }],
    legend: {
      show: true,
    }
  });
  chartReportSeries: ApexAxisChartSeries = [];

  constructor(public service:SalesReportService) {
    this.filter.bolgeIdList = [];
    this.filter.cikisDepoIdList = [];
    this.filter.koordinatorIdList = [];
    this.filter.teslimatTipiIdList = [];
    this.filter.varisDepoIdList = [];
  }

  ngOnInit() {

    this.service.GetCoordinatorList().then(result => {
      if(result.success)
      {
        this.coordionatorList = result.datas;
      }
    }).catch(() => {
      alert("error");
    });;

    this.service.GetDistrictList().then(result => {
      if(result.success)
      {
        this.districtList = result.datas;
      }
    }).catch(() => {
      alert("error");
    });;

    this.service.GetWarehouseList().then(result => {
      if(result.success)
      {
        this.warehouseList = result.datas;
      }
    }).catch(() => {
      alert("error");
    });;

    this.service.GetDeliveryTypeList().then(result => {
      if(result.success)
      {
        this.deliveryTypeList = result.datas;
      }
    }).catch(() => {
      alert("error");
    });;

  }

  filterUpdate()
  {
    if(this.chartViewBtnGroup.value == this.chartViewEnum.Day){
      this.service.GetDailyReportList(this.filter).then(result => {
        if(result.success)
        {
          this.setDatateble(result.datas, result.datas.length);
          this.setChart(result.datas);
        }
      }).catch(() => {
        alert("error");
      });;
    }
    else if (this.chartViewBtnGroup.value == this.chartViewEnum.Mount)
    {
      this.service.GetMonthlyReportList(this.filter).then(result => {
        if(result.success)
        {
          this.setDatateble(result.datas, result.datas.length);
          this.setChart(result.datas);
        }
      }).catch(() => {
        alert("error");
      });
    }
  }

  setDatateble(datas:Report[],lenght:number){
    this.dataSourceReportList = new MatTableDataSource<Report>(datas);
    this.dataSourceReportList.paginator = this.paginator;
    this.paginator.length = lenght;
  }

  setChart(datas:Report[]){
    this.reportChart.chart.destroy();
    this.chartReportSeries = [];
    let labels = datas.map(function(item){
      return item.day;
    });
    labels.pop();
    this.chartReportOptions.labels = labels;

    let yearTotal = datas.map(function(item){
      return item.currentYearTotal;
    });
    yearTotal.pop();
    this.chartReportSeries.push({
      name:'Bu Yıl Toplamlar',
      data: yearTotal,
    });

    const cumulativeSum = (sum => value => sum += value)(0);
    let cumulativeTotal = datas.map(function(item){
      return cumulativeSum(item.currentYearTotal);
    });
    cumulativeTotal.pop();
    this.chartReportSeries.push({
      name:'Bu Yıl Kümülatif Toplamlar',
      data: cumulativeTotal,
    });

    this.reportChart.series = this.chartReportSeries;
    this.reportChart.render();
  }

  selectAll = (modelName:string, dataName:string, label:string) => {
    this.filter[modelName] = this[dataName].map(x => x[label]);
  }
  unselectAll = (modelName:string) => {
    this.filter[modelName] = [];
  }

  applyTableFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceReportList.filter = filterValue.trim().toLowerCase();
  }

}
