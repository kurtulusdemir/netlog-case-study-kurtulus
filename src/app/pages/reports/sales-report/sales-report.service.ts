import { Injectable } from '@angular/core';
import { ServiceBase } from '../../../services/base.service'
import { ApiUrl } from '../../../services/api.service';
import { SalesReportFilter, Report } from './sales-report.model';
 
@Injectable({
  providedIn: 'root'
})
export class SalesReportService extends ServiceBase {
 
  public GetCoordinatorList = () => {
    return this.GetList<any>(ApiUrl.CoordinatorList);
  }

  public GetDistrictList = () => {
    return this.GetList<any>(ApiUrl.DistrictList);
  }

  public GetWarehouseList = () => {
    return this.GetList<any>(ApiUrl.WarehouseList);
  }

  public GetDeliveryTypeList = () => {
    return this.GetList<any>(ApiUrl.DeliveryTypeList);
  }

  public GetDailyReportList = (filter: SalesReportFilter) => {
    return this.GetListByFilter<Report>(filter, ApiUrl.DailyReportList);
  }

  public GetMonthlyReportList = (filter: SalesReportFilter) => {
    return this.GetListByFilter<Report>(filter, ApiUrl.MonthlyReportList);
  }
 
}
