let appInjectorRef;

export const appInjector = (injector?:any) => {

    if(!injector) {
        return appInjectorRef;
    }
    appInjectorRef = injector;
    return appInjectorRef;

};