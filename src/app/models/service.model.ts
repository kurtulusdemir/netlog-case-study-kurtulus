import { ApiResponseList, DataResultModel, ApiDataResponse, ApiDataResponseList, ApiResponse, ApiResponseSingle } from './api.model';

export class ServiceResultSingle<T> {
    public data: T;
    public success: boolean = false;
    public errorCode: string;
    public message: string;
    public url: string;

    constructor(private res: ApiResponseSingle<T> | any) {
        var r = typeof res;
        if (r == "object") {
            if (res.ok) {
                this.success = true;
                this.data = res.data;
            }
            else {
                this.success = false;
                this.errorCode = res.errorCode;
                this.message = res.message;
                this.url = res.url;
            }
        }
        else {
            this.success = false;
            this.errorCode = res.errorCode;
            this.message = res.message;
            this.url = res.url;
        }
    }
}

export class ServiceResultList<T> {
    public datas: Array<T>;
    public success: boolean = false;
    public errorCode: string;
    public message: string;
    public url: string;

    constructor(private res: ApiResponseList<T> | any) {
        var r = typeof res;
        if (r == "object") {
            if (res.success) {
                this.success = true;
                this.datas = res.data;
            }
            else {
                this.success = false;
                this.errorCode = res.errorCode;
                this.message = res.message;
                this.url = res.url;
            }
        }
        else {
            this.success = false;
            this.errorCode = res.errorCode;
            this.message = res.message;
            this.url = res.url;
        }
    }
}

export class ServiceDataResult<T> {
    public data: DataResultModel<T>;
    public success: boolean = false;
    public errorCode: string;
    public message: string;
    public url: string;

    constructor(private res: ApiDataResponse<T> | any) {
        var r = typeof res;
        if (r == "object") {
            if (res.ok) {
                this.success = true;
                this.data = res.data;
            }
            else {
                this.success = false;
                this.errorCode = res.errorCode;
                this.message = res.message;
                this.url = res.url;
            }
        }
        else {
            this.success = false;
            this.errorCode = res.errorCode;
            this.message = res.message;
            this.url = res.url;
        }
    }
}

export class ServiceDataResultList<T> {
    public datas: Array<DataResultModel<T>>;
    public success: boolean = false;
    public errorCode: string;
    public message: string;
    public url: string;

    constructor(private res: ApiDataResponseList<T> | any) {
        var r = typeof res;
        if (r == "object") {
            if (res.ok) {
                this.success = true;
                this.datas = res.data;
            }
            else {
                this.success = false;
                this.errorCode = res.errorCode;
                this.message = res.message;
                this.url = res.url;
            }
        }
        else {
            this.success = false;
            this.errorCode = res.errorCode;
            this.message = res.message;
            this.url = res.url;
        }
    }
}

export class ServiceResult<T> {
    public success: boolean = false;
    public errorCode: string;
    public message: string;
    public url: string;

    constructor(private res: ApiResponse | any) {
        var r = typeof res;
        if (r == "object") {
            if (res.ok) {
                this.success = true;
            }
            else {
                this.success = false;
                this.errorCode = res.errorCode;
                this.message = res.message;
                this.url = res.url;
            }
        }
        else {
            this.success = false;
            this.errorCode = res.errorCode;
            this.message = res.message;
            this.url = res.url;
        }
    }
}

