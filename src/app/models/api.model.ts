 export class ApiResponse {
    public success: boolean;
    public errorCode: string;
    public message: string;
    public url: number;
  }
  
  export class ApiResponseList<T> extends ApiResponse {
    public data: Array<T>;
  }
  
  export class ApiResponseSingle<T> extends ApiResponse {
    public data: T;
  }
  
  export class ApiDataResponse<T> extends ApiResponse {
    public data: DataResultModel<T>;
  }
  
  export class ApiDataResponseList<T> extends ApiResponse {
    public data: Array<DataResultModel<T>>;
  }
 
  export class DataResultModel<T>{
    public success: boolean;
    public errorCode: string;
    public message: string;
    public url: number;
    public data: T;  
  }
 

