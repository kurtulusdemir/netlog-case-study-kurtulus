import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/user';
import { ApiService, ApiUrl } from './api.service';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient, private router: Router,) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(ApiUrl.Base + ApiUrl.UserLogin, { username, password })
            .pipe(map(user => {

                if (user && user.success && user.data && user.data.accessToken) {
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                    localStorage.setItem('currentUserToken', JSON.stringify(user.data.accessToken));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    public getToken(): string {
        return JSON.parse(localStorage.getItem('currentUserToken'));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUserToken');
        this.currentUserSubject.next(null);
        this.router.navigate(['/login']);
    }
}