import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiResponseList, ApiResponseSingle, ApiDataResponse, ApiResponse, ApiDataResponseList } from '../models/api.model';
 
@Injectable()
export class ApiService {
 
  constructor(private http: HttpClient) {}
 
  GetList<T>(path: string): Promise<ApiResponseList<T>> {
    return <Promise<ApiResponseList<T>>>this.http.post(ApiUrl.getApiUrlPath(path), this.authHttpOptions).toPromise();
  }
 
  GetListByFilter<T>(filter: any, path: string): Promise<ApiResponseList<T>> {
    return <Promise<ApiResponseList<T>>>this.http.post(ApiUrl.getApiUrlPath(path), JSON.stringify(filter), this.authHttpOptions).toPromise();
  }
 
  GetSingle<T>(key: any, path: string): Promise<ApiResponseSingle<T>> {
    let httpOptions = this.authHttpOptions;
    let params: HttpParams = new HttpParams();
    params = params.set("id", key);
    httpOptions["params"] = params;
    return <Promise<ApiResponseSingle<T>>>this.http.get(ApiUrl.getApiUrlPath(path), httpOptions).toPromise();
  }
 
  GetSingleByFilter<T>(filter: any, path: string): Promise<ApiResponseSingle<T>> {
    let httpOptions = this.authHttpOptions;
    httpOptions["params"] = filter;
    return <Promise<ApiResponseSingle<T>>>this.http.get(ApiUrl.getApiUrlPath(path), httpOptions).toPromise();
  }
 
  GetSingleByFilterPost<T>(filter: any, path: string): Promise<ApiResponseSingle<T>> {
    return <Promise<ApiResponseSingle<T>>>this.http.post(ApiUrl.getApiUrlPath(path), JSON.stringify(filter), this.authHttpOptions).toPromise();
  }
 
  Insert<T>(model: T, path: string): Promise<ApiDataResponse<T>> {
    return <Promise<ApiDataResponse<T>>>this.http.post(ApiUrl.getApiUrlPath(path), JSON.stringify(model), this.authHttpOptions).toPromise();
  }
 
  Update<T>(model: T, path: string): Promise<ApiDataResponse<T>> {
    return <Promise<ApiDataResponse<T>>>this.http.put(ApiUrl.getApiUrlPath(path), JSON.stringify(model), this.authHttpOptions).toPromise();
  }
 
  Delete<T>(id: number, path: string): Promise<ApiResponse> {
    return <Promise<ApiResponse>>this.http.delete(ApiUrl.getApiUrlPath(path) + "/" + id.toString(), this.authHttpOptions).toPromise();
  }
 
  UpdateBulk<T>(entites: T[], path: string): Promise<ApiDataResponseList<T>> {
    return <Promise<ApiDataResponseList<T>>>this.http.post<ApiDataResponseList<T>>(ApiUrl.getApiUrlPath(path), JSON.stringify(entites), this.authHttpOptions).toPromise();
  }
 
  PostAny<T>(model: T, path: string): Promise<ApiResponseSingle<boolean>> {
    return <Promise<ApiResponseSingle<boolean>>>this.http.post(ApiUrl.getApiUrlPath(path), JSON.stringify(model), this.authHttpOptions).toPromise();
  }
 
  Post<T, T1>(model: T, path: string): Promise<ApiResponseSingle<T1>> {
    return <Promise<ApiResponseSingle<T1>>>this.http.post(ApiUrl.getApiUrlPath(path), JSON.stringify(model), this.authHttpOptions).toPromise();
  }
 
  private get authHttpOptions() {
    var httpOptions =
    {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('currentUserToken'),
        })
    };
    return httpOptions;
  }
}
 
// Api Urls
export class ApiUrl {
 
  public static readonly Base = environment.apiUrl;

  public static readonly UserLogin: string = "User/Login";
  public static readonly CoordinatorList: string = "Koordinator/List";
  public static readonly DistrictList: string = "Bolge/List";
  public static readonly WarehouseList: string = "Depo/List";
  public static readonly DeliveryTypeList: string = "TeslimatTipi/List";
  public static readonly DailyReportList: string = "DailyReport/List";
  public static readonly MonthlyReportList: string = "MonthlyReport/List";


  public static getApiUrlPath(name: string): string {
    return this.Base + name;
  }
 
}
 

