import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service'
import { ServiceResultList, ServiceResultSingle, ServiceDataResult, ServiceResult, ServiceDataResultList } from '../models/service.model';
import { ApiResponseList, ApiResponseSingle } from '../models/api.model';

@Injectable()
export class ServiceBase {

    constructor(public api: ApiService) {}

    protected GetList = <T>(api: string) => {
        return new Promise<ServiceResultList<T>>((resolve, reject) => {
            this.api.GetList<ApiResponseList<T>>(api).then(x => {
                resolve(new ServiceResultList<T>(x));
            }).catch((error: Response | any) => {
                reject(new ServiceResultList<T>(error.message || error));
            });
        });
    }

    protected GetListByFilter = <T>(d: any, api: string) => {
        return new Promise<ServiceResultList<T>>((resolve, reject) => {
            this.api.GetListByFilter<ApiResponseList<T>>(d, api).then(x => {
                resolve(new ServiceResultList<T>(x));
            }).catch((error: Response | any) => {
                reject(new ServiceResultList<T>(error.message || error));
            });
        });
    }

    protected GetSingle = <T>(key: any, api: string) => {
        return new Promise<ServiceResultSingle<T>>((resolve, reject) => {
            this.api.GetSingle<ApiResponseSingle<T>>(key, api).then(x => {
                resolve(new ServiceResultSingle<T>(x));
            }).catch((error: Response | any) => {
                reject(new ServiceResultSingle<T>(error.message || error));
            });
        });
    }

    protected GetSingleByFilter = <T>(filter: any, api: string) => {
        return new Promise<ServiceResultSingle<T>>((resolve, reject) => {
            this.api.GetSingleByFilter<ApiResponseSingle<T>>(filter, api).then(x => {
                resolve(new ServiceResultSingle<T>(x));
            }).catch((error: Response | any) => {
                reject(new ServiceResultSingle<T>(error.message || error));
            });
        });
    }

    protected Add = <T>(api: string, d: T) => {
        return new Promise<ServiceDataResult<T>>((resolve, reject) => {
            this.api.Insert<T>(d, api)
                .then(x => {
                    resolve(new ServiceDataResult(x));
                }).catch((error: Response | any) => {
                    reject(new ServiceDataResult<T>(error.message || error));
                });
        });
    }

    protected PostAny = <T extends any>(api: string, d: T): Promise<ServiceResult<boolean>> => {
        return new Promise<ServiceResult<boolean>>((resolve, reject) => {
            this.api.PostAny<T>(d, api)
                .then(x => {
                    resolve(new ServiceResult(x));
                }).catch((error: Response | any) => {
                    reject(new ServiceResult<boolean>(error.message || error));
                });
        });
    }

    protected Post = <T, T1 extends any>(api: string, d: T): Promise<ServiceResultSingle<T1>> => {
        return new Promise<ServiceResultSingle<T1>>((resolve, reject) => {
            this.api.Post<T, T1>(d, api)
                .then(x => {
                    resolve(new ServiceResultSingle(x));
                }).catch((error: Response | any) => {
                    reject(new ServiceResultSingle<T1>(error.message || error));
                });
        });
    }

    protected Update = <T>(api: string, d: T) => {
        return new Promise<ServiceDataResult<T>>((resolve, reject) => {
            this.api.Update<T>(d, api)
                .then(x => {
                    resolve(new ServiceDataResult(x));
                }).catch((error: Response | any) => {
                    reject(new ServiceDataResult<T>(error.message || error));
                });
        });
    }

    protected Delete = (api: string, id: number) => {
        return new Promise<ServiceResult<number>>((resolve, reject) => {
            this.api.Delete<number>(id, api)
                .then(x => {
                    resolve(new ServiceResult(x));
                }).catch((error: Response | any) => {
                    reject(new ServiceResult<number>(error.message || error));
                });
        });
    }

    protected UpdateBulk = <T>(api: string, d: T[]) => {
        return new Promise<ServiceDataResultList<T>>((resolve, reject) => {
            this.api.UpdateBulk<T>(d, api)
                .then(x => {
                    resolve(new ServiceDataResultList(x));
                }).catch((error: Response | any) => {
                    reject(new ServiceDataResultList<T>(error.message || error));
                });
        });
    }

}



